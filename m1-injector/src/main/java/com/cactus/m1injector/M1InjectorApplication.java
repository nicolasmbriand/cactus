package com.cactus.m1injector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class M1InjectorApplication {

	public static void main(String[] args) {
		SpringApplication.run(M1InjectorApplication.class);
	}

}
