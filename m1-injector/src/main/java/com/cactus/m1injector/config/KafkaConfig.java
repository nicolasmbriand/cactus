package com.cactus.m1injector.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaAdmin;

@Configuration
public class KafkaConfig {
    @Bean
    public KafkaAdmin.NewTopics topicsDefault() {
        return new KafkaAdmin.NewTopics(
                TopicBuilder.name("defaultSimple")
                        .build(),
                TopicBuilder.name("defaulReplicas")
                        .replicas(1)
                        .build(),
                TopicBuilder.name("defaultPartitions")
                        .partitions(3)
                        .build());
    }
}
