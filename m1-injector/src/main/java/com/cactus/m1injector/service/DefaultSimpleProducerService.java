package com.cactus.m1injector.service;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DefaultSimpleProducerService {

    @Value("${kafka.topics.defaultSimple}")
    private String defaultSimpleTopic;

    private final KafkaProducer<String, String> defaultSimpleProducer;

    public void sendDefaultSimple(String message) {
        ProducerRecord<String, String> producerRecord = new ProducerRecord<>(this.defaultSimpleTopic, message);
        this.defaultSimpleProducer.send(producerRecord);
    }
}
