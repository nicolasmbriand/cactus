package com.cactus.m1injector.service;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class DefaultSimpleProducerTest {

    @Mock
    private KafkaProducer<String, String> kafkaProducer;

    @InjectMocks
    private DefaultSimpleProducerService defaultSimpleProducerService;

    @BeforeEach
    void setUp() {
        ReflectionTestUtils.setField(this.defaultSimpleProducerService, "defaultSimpleTopic", "defaultTopic");
    }

    @Test
    void sendDefaultSimple_test() {
        String stringExpected = "stringExpected";
        defaultSimpleProducerService.sendDefaultSimple(stringExpected);
        verify(kafkaProducer, times(1)).send(Mockito.any());
    }
}
