package com.cactus.m2filter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class M2FilterApplication {

	public static void main(String[] args) {
		SpringApplication.run(M2FilterApplication.class, args);
	}

}
